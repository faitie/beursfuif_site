/**
 * @author Faitie
 */
var app = require("express")(),
	express = require('express'),
	server = require('http').createServer(app),
 	io = require('socket.io').listen(server),
 	__und = require('underscore'),
 	_stock = require('./js/bar');
 	
// all environments
app.configure(function(){
  app.set('title', 'Beursfuif');
});
 	
server.listen(80);

//routing
app.get('/',function(req,res){
	//app.use("/css", express.static(__dirname + '/css'));
	app.use("/js", express.static(__dirname + '/js'));
	res.sendfile(__dirname + '\\barman.html');
});

io.sockets.on('connection', function(socket){
	var test = new _stock();
	
	// when client emits 'addItem' , this listens and executes
	socket.on('addItem', function(_name, _currentPrice, _minPrice, _maxPrice){
		test.addItem(_name, _currentPrice, _minPrice, _maxPrice);
	});
	
	// when client emits 'addItem' , this listens and executes
	socket.on('log', function(){
		io.sockets.emit('writelog', test.print());
	});
		
});



