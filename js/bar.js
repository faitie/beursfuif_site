/**
 * @author Faitie
 */
//The bar contains multiple items stored in the itemMap

//Item object contains item id, item name, item price, min price, max price
var item = function()
{
	this.id = 0;
	this.name = "";
	this.currentPrice = 0.00;
	this.minPrice = 0.00;
	this.maxPrice = 9.99;
};

//Itemmap
function stock()
{
	this.items = new Array();
};

//get amount of items in bar.
stock.prototype.getCount = function(){
	return this.items.length;
};

//get new stock ID
stock.prototype.getID = function()
{
	var prevID = 0;
	for(x in this.items){
		prevID = x.id;
		if(x.id > prevID){prevID = x.id;};
	};
	return prevID;
};

//add new item to the stock
stock.prototype.addItem = function(_name, _currentPrice, _minPrice, _maxPrice){
	//create new item
	var _item = new item();
	var id = this.getID();
	//var id = stock.getID();
	_item.id = id;
	_item.name = _name;
	_item.currentPrice = _currentPrice;
	_item.minPrice = _minPrice;
	_item.maxPrice = _maxPrice;
	//push new item onto array
	this.items.push(_item);	
};

//print all objects for debugging purposes
stock.prototype.print = function(){
	for(var j = 0; j < this.getCount(); j++){
		return(this.items[j]);
	};
};

//export the stock constructor from this module.
module.exports = stock;

